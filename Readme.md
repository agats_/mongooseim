Requires installing: [MongooseIM](https://www.erlang-solutions.com/downloads/download-mongooseim)


To run server launch:
<pre>
    sudo ./start.sh
</pre>
(if doesn't work adjust paths in that file)


To deploy module:
<pre>
    sudo ./src/publish.sh
</pre>
(if doesn't work adjust paths in that file)

To let Mongoose use module one should modify <Mongoose instalation dir>/etc/ejabberd.cfg:
<pre>
{modules,
 [
  {packet_interceptor, []},
  ...
  ]
}
</pre>

Pre-created users:

+ user1@localhost (password:user1)
+ user2@localhost (password:user2)


You need an XMPP client to test the server (like Empathy on  ubuntu)
port: 5222
use no ssl/tls
