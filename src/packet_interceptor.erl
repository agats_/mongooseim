-module(packet_interceptor).
-behaviour(gen_mod).
-export([start/2, stop/1]).
-export([on_filter_packet/1]).

-include("ejabberd.hrl").
-include("exml.hrl").

start(Host, _Opts) ->
  ejabberd_hooks:add(filter_packet, global, ?MODULE, on_filter_packet, 0).

stop(Host) ->
  ejabberd_hooks:delete(filter_packet, global, ?MODULE, on_filter_packet, 0).

on_filter_packet({From, To, Packet}) ->
  TypeAttribute = xml:get_tag_attr_s(<<"type">>, Packet),
  FromAttribute = xml:get_tag_attr_s(<<"from">>, Packet),
  ToAttribute = xml:get_tag_attr_s(<<"to">>, Packet),
  ValidatedAttribute = xml:get_tag_attr_s(<<"validated">>, Packet),
  BodyCdata = xml:get_path_s(Packet, [{elem, <<"body">>}, cdata]),

  case {TypeAttribute, BodyCdata, ValidatedAttribute} of
    {<<"chat">>, <<>>, _} -> {From, To, Packet};
    {<<"chat">>, _, <<"true">>} -> {From, To, Packet};
    {<<"chat">>, _ ,_} ->
      NewBodyBinary = getText(BodyCdata),
      NewPacket = Packet#xmlel{
        name = <<"message">>,
        attrs = [{<<"xml:lang">>, <<>>},
          {<<"type">>, TypeAttribute},
          {<<"from">>, FromAttribute},
          {<<"to">>, ToAttribute},
          {<<"validated">>, <<"true">>}],
        children = [
          #xmlel{name = <<"body">>,
            attrs = [],
            children = [
              #xmlcdata{content = NewBodyBinary}
            ]}
        ]},
      ejabberd_router:route(From, To, NewPacket),
      drop;
    _ -> {From, To, Packet}
  end.

getText(Binary) ->
  B = binary:replace(Binary,<<"fuck">>,<<"duck">>,[]),
  B2 = binary:replace(B,<<"ass">>,<<"bass">>,[]),
  B3 = binary:replace(B2,<<"shit">>,<<"poo">>,[]),
  B4 = binary:replace(B3,<<"kurde">>,<<"kurczaki">>,[]),
  binary:replace(B4,<<"psia kość">>,<<"motyla noga!">>,[]).
