#!/bin/bash

EJABBERD_HOME=/usr/lib/mongooseim/lib/ejabberd-2.1.8+mim-1.5.0
EXML_HOME=/usr/lib/mongooseim/lib/exml-2.1.5
MONGOOSE_HOME=/usr/lib/mongooseim/bin/

erlc -I $EXML_HOME/include/ -I $EJABBERD_HOME/include/ *.erl
cp -f *.beam $EJABBERD_HOME/ebin

$MONGOOSE_HOME/mongooseim restart